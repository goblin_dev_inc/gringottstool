package me.staff4.GringottsTool.Cron;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public  final class Utils {
    private Utils() { }

    public static  <T> Optional<T> getElement(final List<T> list, final int index) {
        if (isEmpty(list)) {
            return Optional.empty();
        }
        if (index < 0 || index >= list.size()) {
            return Optional.empty();
        }
        return Optional.of(list.get(index));
    }

    public static boolean isEmpty(final Collection<?> c) {
        return c == null || c.isEmpty();
    }
}
