package me.staff4.GringottsTool;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import me.staff4.GringottsTool.DTO.IncomingMessage;
import me.staff4.GringottsTool.DTO.IncomingMessageType;
import me.staff4.GringottsTool.DTO.OutgoingMessage;
import me.staff4.GringottsTool.DTO.OutgoingMessageType;
import me.staff4.GringottsTool.Exeptions.HealthExeption;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.starter.SpringWebhookBot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.BlockingQueue;

@Slf4j
public final class Bot extends SpringWebhookBot implements Runnable, Healthcheckable {

    private final BlockingQueue<IncomingMessage> inQueue;
    private final BlockingQueue<OutgoingMessage> outQueue;
    @Setter
    private String botPath;
    @Setter
    private String botUserName;
    @Setter
    private String botToken;

    private static final long TIME_DIVISOR = 1000L;
    private static final int MINUTE = 5;
    private static final long SECOND_IN_1_MINUTE = 60L;

    public Bot(final SetWebhook setWebhook, final BlockingQueue<IncomingMessage> inQueue,
               final BlockingQueue<OutgoingMessage> outQueue) {
        super(setWebhook);
        this.inQueue = inQueue;
        this.outQueue = outQueue;
    }

    public void reportStartMessage() {
        executeMessage(Constants.START_MESSAGE, Constants.ADMIN_CHAT_ID);
    }

    @Override
    public String getBotUsername() {
        return botUserName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                OutgoingMessage outgoingMessage = outQueue.take();
                if (outgoingMessage.getType() == OutgoingMessageType.POLL) {
                    sendPollx(outgoingMessage);
                } else if (outgoingMessage.getType() == OutgoingMessageType.EDIT) {
                    execute(sendEditMessage(outgoingMessage));
                } else {
                    SendMessage sendMessage = buildSendMessage(outgoingMessage);
                    execute(sendMessage);
                }
                if (outgoingMessage.isHasDocument()) {
                    String filePath = outgoingMessage.getDocumentFilePath();
                    SendDocument sendDocument = new SendDocument(outgoingMessage.getChatId(),
                            new InputFile(new File(filePath)));
                    sendDocument.setCaption(outgoingMessage.getDocumentFileName());
                    execute(sendDocument);
                    Files.delete(Paths.get(filePath));
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.info(Constants.ERROR_TAKING_IN_BOT, e);
            } catch (TelegramApiException e) {
                log.info(Constants.ERROR_SEND_MESSAGE_TG, e);
            } catch (IOException e) {
                log.info(Constants.ERROR_DELETING_TEMP_FILE, e);
            }
        }
    }

    private SendMessage buildSendMessage(OutgoingMessage outgoingMessage) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(outgoingMessage.getChatId());
        sendMessage.setText(outgoingMessage.getText());
        sendMessage.setReplyToMessageId(outgoingMessage.getReplyToMessageId());
        if (outgoingMessage.isEnableMarkdown()) {
            sendMessage.enableMarkdown(true);
        } else {
            sendMessage.setParseMode(outgoingMessage.getParseMode());
        }
        return sendMessage;
    }

    private void sendPollx(final OutgoingMessage outgoingMessage) throws TelegramApiException, InterruptedException {
        SendMessage sendMessage = new SendMessage(Constants.PUBLIC_CHAT_ID, outgoingMessage.getText());
        sendMessage.setReplyMarkup(outgoingMessage.getInlineKeyboardMarkup());
        sendMessage.enableMarkdown(true);
        Message pollMessage = execute(sendMessage);
        inQueue.put(IncomingMessage.builder().type(IncomingMessageType.POLL).
                userTgId(Long.parseLong(outgoingMessage.getUserTgId())).
                messageId(pollMessage.getMessageId()).
                build());
    }

    private EditMessageText sendEditMessage(final OutgoingMessage outgoingMessage) {
        EditMessageText editMessage = new EditMessageText();
        editMessage.setChatId(outgoingMessage.getChatId());
        editMessage.setMessageId(outgoingMessage.getPollMessageId());
        editMessage.setText(outgoingMessage.getText());
        editMessage.setReplyMarkup(outgoingMessage.getInlineKeyboardMarkup());
        editMessage.enableMarkdown(true);
        return editMessage;
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(final Update update) {
        if (update.hasCallbackQuery()) {
            handleCallbackQuery(update);
        } else {
            if (!update.hasMessage()) {
                return null;
            }
            Message message = update.getMessage();
            String chatId = message.getChatId().toString();
            long userTgId = message.getFrom().getId();
            String text = message.getText();
            log.debug("Received message: " + text);
            if (checkTimeMessage(message.getDate())) {
                return null;
            }
            if (text == null) {
                return null;
            }
            try {
                IncomingMessage incomingMessage = IncomingMessage.builder().
                        type(IncomingMessageType.COMMAND).
                        chatId(chatId).
                        userTgId(userTgId).
                        text(text).
                        messageId(message.getMessageId()).
                        build();
                inQueue.put(incomingMessage);
            } catch (InterruptedException e) {
            executeMessage(Constants.ERROR_OUT_WRITE_IN_BOT, Constants.ADMIN_CHAT_ID);
            }
        }
        return null;
    }

    private void handleCallbackQuery(final Update update) {
        try {
            CallbackQuery callbackQuery = update.getCallbackQuery();
            IncomingMessage incomingMessage = IncomingMessage.builder().
                    type(IncomingMessageType.CALLBACKQUERY).
                    chatId(String.valueOf(callbackQuery.getMessage().getChatId())).
                    userTgId(callbackQuery.getFrom().getId()).
                    callbackQueryData(callbackQuery.getData()).
                    pollMessageId(callbackQuery.getMessage().getMessageId()).
                    text(callbackQuery.getMessage().getText()).
                    build();
            inQueue.put(incomingMessage);
        } catch (InterruptedException e) {
            executeMessage(Constants.ERROR_OUT_WRITE_IN_BOT, Constants.ADMIN_CHAT_ID);
        }
    }

    private boolean checkTimeMessage(final int messageDate) {
        return messageDate + MINUTE * SECOND_IN_1_MINUTE < (System.currentTimeMillis() / TIME_DIVISOR);
    }

    @Override
    public String getBotPath() {
        return botPath;
    }


    private void executeMessage(final String text, final String chatID) {
        try {
            SendMessage message = new SendMessage();
            message.setChatId(chatID);
            message.setParseMode(ParseMode.HTML);
            message.setText(text);
            execute(message);
        } catch (TelegramApiException e) {
            log.error("error: ", e);
        }
    }

    @Override
    public void isAlive() throws HealthExeption {
        try {
            Message sendMessage = execute(new SendMessage(Constants.ADMIN_CHAT_ID, "Проверка"));
            execute(new DeleteMessage(Constants.ADMIN_CHAT_ID, sendMessage.getMessageId()));
            log.info("Bot is healthy");
        } catch (TelegramApiException e) {
            throw new HealthExeption("There is a problem with Bot");
        }
    }
}
