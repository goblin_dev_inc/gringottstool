package me.staff4.GringottsTool.Entities.H2Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "data")
public class Vote {
    @Id
    @Column
    private String id;
    @Column
    private String messageId;
    @Column
    private String name;
    @Column
    private String answer;
    @Column
    private String pollQuestion;
    @Column
    private Date date;
}
