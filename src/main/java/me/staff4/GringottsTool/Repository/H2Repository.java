package me.staff4.GringottsTool.Repository;

import me.staff4.GringottsTool.Entities.H2Entities.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface H2Repository extends JpaRepository<Vote, String> {
    ArrayList<Vote> findAllByMessageId(String messageId);
}
