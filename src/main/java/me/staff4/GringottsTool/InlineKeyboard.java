package me.staff4.GringottsTool;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class InlineKeyboard {
    @Getter
    @Setter
    private HashMap<String, Integer> answerCounter;
    @Getter
    @Setter
    private String statisticMessageId;
    public final InlineKeyboardMarkup getInlineMessageButtons(final String[] answers) {
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        for (String text : answers) {
            rowList.add(getButton(text, text));
        }
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(rowList);
        return inlineKeyboardMarkup;
    }

    private List<InlineKeyboardButton> getButton(final String buttonName, final String buttonCallBackData) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(buttonName);
        button.setCallbackData(buttonCallBackData);
        List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<>();
        keyboardButtonsRow.add(button);
        return keyboardButtonsRow;
    }
}
