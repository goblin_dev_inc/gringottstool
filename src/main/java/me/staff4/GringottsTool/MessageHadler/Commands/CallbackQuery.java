package me.staff4.GringottsTool.MessageHadler.Commands;

import me.staff4.GringottsTool.Constants;
import me.staff4.GringottsTool.DTO.IncomingMessage;
import me.staff4.GringottsTool.DTO.OutgoingMessage;
import me.staff4.GringottsTool.DTO.OutgoingMessageType;
import me.staff4.GringottsTool.Entities.Partner;
import me.staff4.GringottsTool.Exeptions.InvalidDataException;
import me.staff4.GringottsTool.Exeptions.NoDataFound;
import me.staff4.GringottsTool.InlineKeyboard;
import me.staff4.GringottsTool.Repository.H2Repository;
import me.staff4.GringottsTool.MessageHadler.Commands.Interfaces.MessageCommandExecutorResponder;
import me.staff4.GringottsTool.MessageHadler.Commands.Interfaces.PublicMessageCommandExecutor;
import me.staff4.GringottsTool.MessageHadler.MessageCommand;
import me.staff4.GringottsTool.Entities.H2Entities.Vote;
import me.staff4.GringottsTool.Templates.TemplateEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public final class CallbackQuery extends AbsGetCommand implements PublicMessageCommandExecutor {
    @Autowired
    private H2Repository h2Repository;
    @Autowired
    private InlineKeyboard inlineKeyboard;

    @Override
    public MessageCommand command() {
        return MessageCommand.CALLBACKQUERY;
    }

    @Override
    public void execute(final MessageCommandExecutorResponder responder, final IncomingMessage incomingMessage)
            throws InvalidDataException, NoDataFound, IOException, ParseException, GeneralSecurityException {
        long tgId = incomingMessage.getUserTgId();
        if (!getRepository().isPartner(tgId)) {
            return;
        }
        List<Partner> partnerList = getRepository().getPartners(String.valueOf(tgId));
        boolean isInDuckList = partnerList.get(0).isElite();
        if (!isInDuckList) {
            return;
        }
        String name = partnerList.get(0).getName();
        Long messageId = (long) incomingMessage.getPollMessageId();
        long id = tgId + messageId;
        String answer = incomingMessage.getCallbackQueryData();
        Date nowDate = new Date();
        ArrayList<Vote> votes = h2Repository.findAllByMessageId(String.valueOf(messageId));
        String pollQuestion;
        if (votes.isEmpty()) {
            pollQuestion = incomingMessage.getText();
        } else {
            pollQuestion = votes.get(0).getPollQuestion();
        }
        h2Repository.save(new Vote(String.valueOf(id), String.valueOf(messageId), name, answer, pollQuestion, nowDate));
        ArrayList<Vote> allVotes = h2Repository.findAllByMessageId(String.valueOf(messageId));
        String messageText = TemplateEngine.editPollQuestion(pollQuestion, buildStatisticMessage(allVotes));
        String[] answers = new String[]{"За", "Против", "Воздержусь"};
        OutgoingMessage statisticMessage = OutgoingMessage.builder()
                .type(OutgoingMessageType.EDIT)
                .chatId(Constants.PUBLIC_CHAT_ID)
                .text(messageText)
                .pollMessageId(incomingMessage.getPollMessageId())
                .inlineKeyboardMarkup(inlineKeyboard.getInlineMessageButtons(answers))
                .enableMarkdown(true)
                .build();
        responder.put(statisticMessage);
    }

    private String buildStatisticMessage(final ArrayList<Vote> allVotes) {
        StringBuilder yesNames = new StringBuilder();
        StringBuilder noNames = new StringBuilder();
        StringBuilder refrainNames = new StringBuilder();
        int yesCount = 0;
        int noCount = 0;
        int refrainCount = 0;
        for (Vote vote : allVotes) {
            switch (vote.getAnswer()) {
                case "За" -> {
                    yesCount++;
                    yesNames.append("\n|-");
                    yesNames.append(vote.getName());
                }
                case "Против" -> {
                    noCount++;
                    noNames.append("\n|-");
                    noNames.append(vote.getName());
                }
                case "Воздержусь" -> {
                    refrainCount++;
                    refrainNames.append("\n|-");
                    refrainNames.append(vote.getName());
                }
                default -> { }
            }
        }
        String text = TemplateEngine.statisticMessage(yesCount, yesNames.toString(), noCount, noNames.toString(),
                refrainCount, refrainNames.toString());
        return text;
    }
}
